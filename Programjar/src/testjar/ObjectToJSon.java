package testjar;
import java.io.FileWriter;
import java.io.IOException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
// writing json
public class ObjectToJSon 
{

public static void main(String[] args) 
{

//Gson gson= new Gson();
Gson gson= new GsonBuilder().setPrettyPrinting().create();
Employee ed = createmyObject();
System.out.println(ed); // java object printed
String j = gson.toJson(ed); // to convert java object to JSON
System.out.println(j);


try(FileWriter writer = new FileWriter("C:\\Users\\241354\\Desktop\\Jar Files.json")){
gson.toJson(ed, writer);
//System.out.println("ok its done go and check your myfile.json");
}
catch(IOException e) {
e.printStackTrace();
}
}
private static Employee createmyObject() {
	Employee ed= new Employee();
ed.setName("Ranya G P");
ed.setAge(22);
ed.setCity("Banglore");
return ed;

}
}
