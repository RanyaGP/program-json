package testunit;

import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

class MultiplicationTest {

	@Test
	void test() {
		Multiplication m=new Multiplication();
		assertEquals(330,m.multiply(10,33));
	}
	@BeforeEach
	void testAvg() {
		Multiplication m=new Multiplication();
		assertEquals(30,m.avg(20, 40, 30));
	}
	
}
